package vercmp

import (
	"regexp"
	"strings"
)

var naiveFindDigits *regexp.Regexp = regexp.MustCompile("^\\p{Nd}+$")

func naivePartCompare(a string, b string) int {
	aResults := naiveFindDigits.FindStringSubmatch(a)
	bResults := naiveFindDigits.FindStringSubmatch(b)
	if aResults != nil && bResults != nil {
		return strIntCompare(a, b)
	} else if aResults != nil {
		return 1
	} else if bResults != nil {
		return -1
	} else {
		return strings.Compare(a, b)
	}
}

var naiveFindPunct *regexp.Regexp = regexp.MustCompile("\\p{P}+")

func NaiveCompare(a string, b string) int {
	aParts := naiveFindPunct.Split(a, -1)
	bParts := naiveFindPunct.Split(b, -1)
	aLen := len(aParts)
	bLen := len(bParts)
	var minLength int
	if aLen < bLen {
		minLength = aLen
	} else {
		minLength = bLen
	}
	var partCompare int
	for i := 0; i < minLength; i++ {
		partCompare = naivePartCompare(aParts[i], bParts[i])
		if partCompare != 0 {
			return partCompare
		}
	}
	return aLen - bLen
}
