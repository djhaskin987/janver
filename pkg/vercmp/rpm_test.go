package vercmp

import (
	"testing"
)

func TestRpmCompare(t *testing.T) {

	if RpmCompare("", "") != 0 {
		t.Errorf("RpmCompare: `` is equal to ``")
	}
	if RpmCompare("1.2.3", "1.2-3") != 0 {
		t.Errorf("RpmCompare: `1.2.3` is less than `1.2-3`")
	}
	if RpmCompare("1.0010", "1.9") <= 0 {
		t.Errorf("RpmCompare: `1.0010` is greater than `1.9`")
	}
	if RpmCompare("1.05", "1.5") != 0 {
		t.Errorf("RpmCompare: `1.05` is less than `1.5`")
	}
	if RpmCompare("1.0", "1") <= 0 {
		t.Errorf("RpmCompare: `1.0` is greater than `1`")
	}
	if RpmCompare("2.50", "2.5") <= 0 {
		t.Errorf("RpmCompare: `2.50` is greater than `2.5`")
	}
	if RpmCompare("fc4", "fc.4") != 0 {
		t.Errorf("RpmCompare: `fc4` is less than `fc.4`")
	}
	if RpmCompare("FC5", "fc4") >= 0 {
		t.Errorf("RpmCompare: `FC5` is less than `fc4`")
	}
	if RpmCompare("2a", "2.0") >= 0 {
		t.Errorf("RpmCompare: `2a` is less than `2.0`")
	}
	if RpmCompare("2.a", "2a") != 0 {
		t.Errorf("RpmCompare: `2.a` is less than `2a`")
	}
	if RpmCompare("1.0", "1.fc4") <= 0 {
		t.Errorf("RpmCompare: `1.0` is greater than `1.fc4`")
	}
	if RpmCompare("3.0.0_fc", "3.0.0.fc") != 0 {
		t.Errorf("RpmCompare: `3.0.0_fc` is less than `3.0.0.fc`")
	}
	if RpmCompare("~~", "~~a") >= 0 {
		t.Errorf("RpmCompare: `~~` is less than `~~a`")
	}
	if RpmCompare("~", "~~a") <= 0 {
		t.Errorf("RpmCompare: `~` is greater than `~~a`")
	}
	if RpmCompare("~", "") >= 0 {
		t.Errorf("RpmCompare: `~` is less than ``")
	}
	if RpmCompare("a", "") <= 0 {
		t.Errorf("RpmCompare: `a` is greater than ``")
	}
}
