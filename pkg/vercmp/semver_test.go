package vercmp

import (
	"testing"
)

func TestSemverCompare(t *testing.T) {

	if SemverCompare("", "") != 0 {
		t.Errorf("SemverCompare: `` is equal to ``")
	}

	if SemverCompare("1.0.0+001", "1.0.0+20130313144700") != 0 {
		t.Errorf("SemverCompare: `1.0.0+001` is less than `1.0.0+20130313144700`")
	}
	if SemverCompare("1.0.0+exp.sha.5114f85", "1.0.0") != 0 {
		t.Errorf("SemverCompare: `1.0.0+exp.sha.5114f85` is less than `1.0.0`")
	}
	if SemverCompare("1.0.0-alpha", "1.0.0-alpha.1") >= 0 {
		t.Errorf("SemverCompare: `1.0.0-alpha` is less than `1.0.0-alpha.1`")
	}
	if SemverCompare("1.0.0-alpha.1", "1.0.0-alpha.beta") >= 0 {
		t.Errorf("SemverCompare: `1.0.0-alpha.1` is less than `1.0.0-alpha.beta`")
	}
	if SemverCompare("1.0.0-alpha.beta", "1.0.0-beta") >= 0 {
		t.Errorf("SemverCompare: `1.0.0-alpha.beta` is less than `1.0.0-beta`")
	}
	if SemverCompare("1.0.0-beta.2", "1.0.0-beta") <= 0 {
		t.Errorf("SemverCompare: `1.0.0-beta.2` is greater than `1.0.0-beta`")
	}
	if SemverCompare("1.0.0-beta.2", "1.0.0-beta.11") >= 0 {
		t.Errorf("SemverCompare: `1.0.0-beta.2` is less than `1.0.0-beta.11`")
	}
	if SemverCompare("1.0.0-beta.11", "1.0.0-rc.1") >= 0 {
		t.Errorf("SemverCompare: `1.0.0-beta.11` is less than `1.0.0-rc.1`")
	}
	if SemverCompare("1.0.0-rc.1", "1.0.0") >= 0 {
		t.Errorf("SemverCompare: `1.0.0-rc.1` is less than `1.0.0`")
	}
	if SemverCompare("1.0.0", "0.1.0") <= 0 {
		t.Errorf("SemverCompare: `1.0.0` is greater than `0.1.0`")
	}
	if SemverCompare("1.2.10", "1.2.9") <= 0 {
		t.Errorf("SemverCompare: `1.2.10` is greater than `1.2.9`")
	}
	if SemverCompare("9.8.7", "9.8.7+burarum") != 0 {
		t.Errorf("SemverCompare: `9.8.7` is less than `9.8.7+burarum`")
	}
	if SemverCompare("1.0.0", "2.0.0") >= 0 {
		t.Errorf("SemverCompare: `1.0.0` is less than `2.0.0`")
	}
	if SemverCompare("2.0.0", "2.1.0") >= 0 {
		t.Errorf("SemverCompare: `2.0.0` is less than `2.1.0`")
	}
	if SemverCompare("2.1.1", "2.1.0") <= 0 {
		t.Errorf("SemverCompare: `2.1.1` is greater than `2.1.0`")
	}
	if SemverCompare("1.0.0", "1.0.0-alpha") <= 0 {
		t.Errorf("SemverCompare: `1.0.0` is greater than `1.0.0-alpha`")
	}
}
