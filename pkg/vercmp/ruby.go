package vercmp

import (
	"regexp"
)

var findDigitAlpha *regexp.Regexp = regexp.MustCompile(
	"(\\p{Nd}+)(\\p{L}+)")
var findAlphaDigit *regexp.Regexp = regexp.MustCompile(
	"(\\p{L}+)(\\p{Nd}+)")
var findAlphaSuffix *regexp.Regexp = regexp.MustCompile(
	"(\\p{Nd}+(\\.\\p{Nd}+)*)\\.(\\p{L}.*)$")

func RubyNormalize(a string) string {
	a = findDigitAlpha.ReplaceAllString(a, "$1.$2")
	a = findAlphaDigit.ReplaceAllString(a, "$1.$2")
	a = findAlphaSuffix.ReplaceAllString(a, "$1~$3")
	return a
}

func RubyCompare(a string, b string) int {
	return DebianCompare(RubyNormalize(a), RubyNormalize(b))
}
