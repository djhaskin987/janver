package vercmp

import (
	"testing"
)

func TestNaiveCompare(t *testing.T) {

	if NaiveCompare("", "") != 0 {
		t.Errorf("NaiveCompare: `` is equal to ``")
	}
	if NaiveCompare("abc", "abc") != 0 {
		t.Errorf("NaiveCompare: `abc` is less than `abc`")
	}
	if NaiveCompare("a.b.c", "a_b-c") != 0 {
		t.Errorf("NaiveCompare: `a.b.c` is less than `a_b-c`")
	}
	if NaiveCompare("1.2.0", "1.2") <= 0 {
		t.Errorf("NaiveCompare: `1.2.0` is greater than `1.2`")
	}
	if NaiveCompare("1.a", "1.2") >= 0 {
		t.Errorf("NaiveCompare: `1.a` is less than `1.2`")
	}
	if NaiveCompare("0100", "100") != 0 {
		t.Errorf("NaiveCompare: `0100` is less than `100`")
	}
	if NaiveCompare("0100.al0100", "100.al100") >= 0 {
		t.Errorf("NaiveCompare: `0100.al0100` is less than `100.al100`")
	}
	if NaiveCompare("1000.9.17", "100.9.18") <= 0 {
		t.Errorf("NaiveCompare: `1000.9.17` is greater than `100.9.18`")
	}
}
