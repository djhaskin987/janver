package vercmp

import (
	"testing"
)

func TestMavenCompare(t *testing.T) {
	if MavenCompare("1.0.0", "1.0.0") != 0 {
		t.Errorf("MavenCompare: `1.0.0` equals `1.0.0`")
	}
	if MavenCompare("1.0.0", "1.0") != 0 {
		t.Errorf("MavenCompare: `1.0.0` equals `1.0`")
	}
	if MavenCompare("1.0.1", "1.0") <= 0 {
		t.Errorf("MavenCompare: `1.0.1` is greater than `1.0`")
	}
	if MavenCompare("1.0.0", "1.0.1") >= 0 {
		t.Errorf("MavenCompare: `1.0.0` is greater than `1.0.1`")
	}
	if MavenCompare("1.0.0", "0.9.2") <= 0 {
		t.Errorf("MavenCompare: `1.0.0` is greater than `0.9.2`")
	}
	if MavenCompare("0.9.2", "0.9.3") >= 0 {
		t.Errorf("MavenCompare: `0.9.2` is less than `0.9.3`")
	}
	if MavenCompare("0.9.2", "0.9.1") <= 0 {
		t.Errorf("MavenCompare: `0.9.2` is greater than `0.9.1`")
	}
	if MavenCompare("0.9.5", "0.9.13") >= 0 {
		t.Errorf("MavenCompare: `0.9.5` is less than `0.9.13`.")
	}
	if MavenCompare("10.2.0.3.0", "11.2.0.3.0") >= 0 {
		t.Errorf("MavenCompare: `19.2.0.3.0` is less than `11.2.0.3.0`")
	}
	if MavenCompare("10.2.0.3.0", "5.2.0.3.0") <= 0 {
		t.Errorf("MavenCompare: `10.2.0.3.0` is greater than `5.2.0.3.0`")
	}
	if MavenCompare("1.0.0-SNAPSHOT", "1.0.1-SNAPSHOT") >= 0 {
		t.Errorf("MavenCompare: `1.0.0-SNAPSHOT` is less than `1.0.1-SNAPSHOT`")
	}
	if MavenCompare("1.0.0-alpha", "1.0.1-beta") >= 0 {
		t.Errorf("MavenCompare: `1.0.0-alpha` is less than `1.0.1-beta`")
	}
	if MavenCompare("1.1-dolphin", "1.1.1-cobra") >= 0 {
		t.Errorf("MavenCompare: `1.1-dolphin` is less than `1.1.1-cobra`")
	}
	// Lexical Comparison
	if MavenCompare("1.0-alpaca", "1.0-bermuda") >= 0 {
		t.Errorf("MavenCompare: `1.0-alpaca` is less than `1.0-bermuda`")
	}
	if MavenCompare("1.0-alpaca", "1.0-alpaci") >= 0 {
		t.Errorf("MavenCompare: `1.0-alpaca` is less than `1.0-alpaci`")
	}
	if MavenCompare("1.0-dolphin", "1.0-cobra") <= 0 {
		t.Errorf("MavenCompare: `1.0-dolphin` is greater than `1.0-cobra`")
	}

	// Qualifier Comparison
	if MavenCompare("1.0.0-alpha", "1.0.0-beta") >= 0 {
		t.Errorf("MavenCompare: `1.0.0-alpha` is less than `1.0.0-beta`")
	}
	if MavenCompare("1.0.0-beta", "1.0.0-alpha") <= 0 {
		t.Errorf("MavenCompare: `1.0.0-beta` is greater than `1.0.0-alpha`")
	}
	if MavenCompare("1.0.0-alpaca", "1.0.0-beta") >= 0 {
		t.Errorf("MavenCompare: `1.0.0-alpaca` is less than `1.0.0-beta`")
	}
	if MavenCompare("1.0.0-final", "1.0.0-milestone") <= 0 {
		t.Errorf("MavenCompare: `1.0.0-final` is less than `1.0.0-milestone`")
	}

	// Qualifier/Numeric Comparison
	if MavenCompare("1.0.0-alpha1", "1.0.0-alpha2") >= 0 {
		t.Errorf("MavenCompare: `1.0.0-alpha1` is greater than `1.0.0-alpha2`")
	}
	if MavenCompare("1.0.0-alpha5", "1.0.0-alpha23") >= 0 {
		t.Errorf("MavenCompare: `1.0.0-alpha5` is less than `1.0.0-alpha23`")
	}
	if MavenCompare("1.0-RC5", "1.0-RC20") >= 0 {
		t.Errorf("MavenCompare: `1.0-RC5` is less than `1.0-RC20`")
	}
	if MavenCompare("1.0-RC11", "1.0-RC6") <= 0 {
		t.Errorf("MavenCompare: `1.0-RC11` is greater than `1.0-RC6`")
	}

	// Releases are newer than SNAPSHOTs
	if MavenCompare("1.0.0", "1.0.0-SNAPSHOT") <= 0 {
		t.Errorf("MavenCompare: `1.0.0` is less than `1.0.0-SNAPSHOT`")
	}
	if MavenCompare("1.0.0-SNAPSHOT", "1.0.0-SNAPSHOT") != 0 {
		t.Errorf("MavenCompare: `1.0.0-SNAPSHOT` is equal to `1.0.0-SNAPSHOT`")
	}
	if MavenCompare("1.0.0-SNAPSHOT", "1.0.0") >= 0 {
		t.Errorf("MavenCompare: `1.0.0-SNAPSHOT` is greater than `1.0.0`")
	}

	// Releases are newer than qualified versions
	if MavenCompare("1.0.0", "1.0.0-alpha5") <= 0 {
		t.Errorf("MavenCompare: `1.0.0` is less than `1.0.0-alpha5`")
	}
	if MavenCompare("1.0.0-alpha5", "1.0.0") >= 0 {
		t.Errorf("MavenCompare: `1.0.0-alpha5` is greater than `1.0.0`")
	}

	// SNAPSHOTS are newer than qualified versions
	if MavenCompare("1.0.0-SNAPSHOT", "1.0.0-RC1") <= 0 {
		t.Errorf("MavenCompare: `1.0.0-SNAPSHOT` is greater than `1.0.0-RC1`")
	}
	if MavenCompare("1.0.0-SNAPSHOT", "1.0.1-RC1") >= 0 {
		t.Errorf("MavenCompare: `1.0.0-SNAPSHOT` is greater than `1.0.1-RC1`")
	}

	if MavenCompare("9.1-901.jdbc4", "9.1-901.jdbc3") <= 0 {
		t.Errorf("MavenCompare: `9.1-901.jdbc4` is less than `9.1-901.jdbc3`")
	}
	if MavenCompare("9.1-901-1.jdbc4", "9.1-901.jdbc4") <= 0 {
		t.Errorf("MavenCompare: `9.1-901-1.jdbc4` is less than `9.1-901.jdbc4`")
	}

	if MavenCompare("1-SNAPSHOT", "1.0-SNAPSHOT") != 0 {
		t.Errorf("MavenCompare: `1-SNAPSHOT` is equal to `1.0-SNAPSHOT`")
	}
	if MavenCompare("1-alpha", "1-alpha0") != 0 {
		t.Errorf("MavenCompare: `1-alpha` is equal to `1-alpha0`")
	}
}
