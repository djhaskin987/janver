package vercmp

import (
	"testing"
)

func TestRubyCompare(t *testing.T) {
	if RubyCompare("", "") != 0 {
		t.Errorf("RubyCompare: `` is equal to ``")
	}
	if RubyCompare("1.0.0", "0.1.0") <= 0 {
		t.Errorf("RubyCompare: `1.0.0` is greater than `0.1.0`")
	}
	if RubyCompare("1.0.0", "2.0.0") >= 0 {
		t.Errorf("RubyCompare: `1.0.0` is less than `2.0.0`")
	}
	if RubyCompare("1.1.1", "1.1.1") != 0 {
		t.Errorf("RubyCompare: `1.1.1` is equal to `1.1.1`")
	}
	if RubyCompare("2.0.0", "2.1.0") >= 0 {
		t.Errorf("RubyCompare: `2.0.0` is less than `2.1.0`")
	}
	if RubyCompare("2.1.1", "2.1.0") <= 0 {
		t.Errorf("RubyCompare: `2.1.1` is greater than `2.1.0`")
	}
	if RubyCompare("1.0.1", "1.0") <= 0 {
		t.Errorf("RubyCompare: `1.0.1` is greater than `1.0`")
	}
	if RubyCompare("1.0.0", "1.0.1") >= 0 {
		t.Errorf("RubyCompare: `1.0.0` is less than `1.0.1`")
	}
	if RubyCompare("1.0.0", "0.9.2") <= 0 {
		t.Errorf("RubyCompare: `1.0.0` is greater than `0.9.2`")
	}
	if RubyCompare("0.9.2", "0.9.3") >= 0 {
		t.Errorf("RubyCompare: `0.9.2` is less than `0.9.3`")
	}
	if RubyCompare("0.9.2", "0.9.1") <= 0 {
		t.Errorf("RubyCompare: `0.9.2` is greater than `0.9.1`")
	}
	if RubyCompare("0.9.5", "0.9.13") >= 0 {
		t.Errorf("RubyCompare: `0.9.5` is less than `0.9.13`")
	}
	if RubyCompare("10.2.0.3.0", "11.2.0.3.0") >= 0 {
		t.Errorf("RubyCompare: `10.2.0.3.0` is less than `11.2.0.3.0`")
	}
	if RubyCompare("10.2.0.3.0", "5.2.0.3.0") <= 0 {
		t.Errorf("RubyCompare: `10.2.0.3.0` is greater than `5.2.0.3.0`")
	}
	if RubyCompare("1.0", "1") <= 0 {
		t.Errorf("RubyCompare: `1.0` is greater than `1`")
	}
	if RubyCompare("1.2.a", "1.2") >= 0 {
		t.Errorf("RubyCompare: `1.2.a` is less than `1.2`")
	}
	if RubyCompare("1.2.z", "1.2") >= 0 {
		t.Errorf("RubyCompare: `1.2.z` is less than `1.2`")
	}
	if RubyCompare("1.1.z", "1.0") <= 0 {
		t.Errorf("RubyCompare: `1.1.z` is greater than `1.0`")
	}
	if RubyCompare("1.0.a10", "1.0.a9") <= 0 {
		t.Errorf("RubyCompare: `1.0.a10` is greater than `1.0.a9`")
	}
	if RubyCompare("1.0", "1.0.b1") <= 0 {
		t.Errorf("RubyCompare: `1.0` is greater than `1.0.b1`")
	}
	if RubyCompare("1.0.a2", "1.0.b1") >= 0 {
		t.Errorf("RubyCompare: `1.0.a2` is less than `1.0.b1`")
	}
	if RubyCompare("1.0.a2", "0.9") <= 0 {
		t.Errorf("RubyCompare: `1.0.a2` is greater than `0.9`")
	}
	if RubyCompare("1.0.a.10", "1.0.a10") != 0 {
		t.Errorf("RubyCompare: `1.0.a.10` is equal to `1.0.a10`")
	}
	if RubyCompare("1.0.a10", "1.0.a.10") != 0 {
		t.Errorf("RubyCompare: `1.0.a10` is equal to `1.0.a.10`")
	}
}
