package vercmp

import (
	"regexp"
	"strings"
)

var pythonFindEpoch *regexp.Regexp = regexp.MustCompile("^\\p{Nd}+!")
var pythonFindPost *regexp.Regexp = regexp.MustCompile("\\.post(\\p{Nd}+)")
var pythonFindAlpha *regexp.Regexp = regexp.MustCompile("(\\p{Nd}+)(\\p{P})?a(\\p{Nd}+)")
var pythonFindBeta *regexp.Regexp = regexp.MustCompile("(\\p{Nd}+)(\\p{P})?b(\\p{Nd}+)")
var pythonFindRc *regexp.Regexp = regexp.MustCompile("(\\p{Nd}+)(\\p{P})?rc(\\p{Nd}+)")
var pythonFindCrc *regexp.Regexp = regexp.MustCompile("(\\p{Nd}+)(\\p{P})?c(\\p{Nd}+)")
var pythonFindDev *regexp.Regexp = regexp.MustCompile("\\.dev(\\p{Nd}+)")

func PythonNormalize(a string) string {
	a = strings.ToLower(a)
	a = pythonFindEpoch.ReplaceAllString(a, "$1:")
	a = pythonFindPost.ReplaceAllString(a, "!~$1")
	a = pythonFindAlpha.ReplaceAllString(a, "$1~a$3")
	a = pythonFindBeta.ReplaceAllString(a, "$1~b$3")
	a = pythonFindRc.ReplaceAllString(a, "$1~rc$3")
	a = pythonFindCrc.ReplaceAllString(a, "$1~rc$3")
	a = pythonFindDev.ReplaceAllString(a, "~~dev$1")
	return a
}

func PythonCompare(a string, b string) int {
	aParts := strings.Split(a, "+")
	bParts := strings.Split(b, "+")
	debResult := DebianCompare(PythonNormalize(aParts[0]), PythonNormalize(bParts[0]))
	if debResult != 0 {
		return debResult
	}

	if len(aParts) > 1 && len(bParts) > 1 {
		return NaiveCompare(aParts[1], bParts[1])
	} else if len(aParts) > 1 {
		return 1
	} else if len(bParts) > 1 {
		return -1
	} else {
		return 0
	}
}

/*


   (string/replace
     it
     #"^(\p{Digit}+)!"
     "$1:")
   ; 1.0 1.0.post1 1.0.0
   (string/replace
     it
     #"\.post(\p{Digit}+)"
     "!~$1")
   (string/replace
     it
     #"(\p{Digit}+)(\p{Punct})?a(\p{Digit}+)"
     "$1~a$3")
   (string/replace
     it
     #"(\p{Digit}+)(\p{Punct})?b(\p{Digit}+)"
     "$1~b$3")
   (string/replace
     it
     #"(\p{Digit}+)(\p{Punct})?rc(\p{Digit}+)"
     "$1~rc$3")
   (string/replace
     it
     #"(\p{Digit}+)(\p{Punct})?c(\p{Digit}+)"
     "$1~rc$3")
   (string/replace
     it
     #"[.]dev(\p{Digit}+)"
     "~~dev$1")))

*/
