package vercmp

import (
	"regexp"
)

var rpmFindNormalPunct *regexp.Regexp = regexp.MustCompile("[^\\p{L}\\p{N}~]+")
var rpmFindAlphaDigit *regexp.Regexp = regexp.MustCompile("(\\p{L}+)\\.(\\p{Nd}+)")
var rpmFindDigitAlpha *regexp.Regexp = regexp.MustCompile("(\\p{Nd}+)\\.(\\p{L}+)")

func RpmNormalize(a string) string {
	a = rpmFindNormalPunct.ReplaceAllString(a, ".")
	a = rpmFindAlphaDigit.ReplaceAllString(a, "$1$2")
	a = rpmFindDigitAlpha.ReplaceAllString(a, "$1$2")
	return a
}

func RpmCompare(a string, b string) int {
	return DebianCompare(RpmNormalize(a), RpmNormalize(b))
}

/*
(defn rpm-normalize
  "Convert an rpm version number to a debian version number such that
   the function call `(debian-vercmp (rpm-normalize a) (rpm-normalize b))`
   would compare two correct rpm version numbers correctly."
  {:added "1.2.0"}
  [vers]
  (as-> vers it
    (string/replace
     it
     #"(\p{Alpha}+)\.(\p{Digit}+)"
     "$1$2")
    (string/replace
     it
     #"(\p{Digit}+)\.(\p{Alpha}+)"
     "$1$2")))
*/
