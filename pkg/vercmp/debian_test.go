package vercmp

import "testing"

func TestDebianLexicalCompare(t *testing.T) {
	if debianLexicalCompare("", "") != 0 {
		t.Errorf("debianLexicalCompare empty case")
	}
	if debianLexicalCompare("~~a", "~~") <= 0 {
		t.Errorf("debianLexicalCompare: `~~a` is greater than `~~`")
	}
	if debianLexicalCompare("~~a", "~") >= 0 {
		t.Errorf("debianLexicalCompare: `~~a` is less than `~`")
	}
	if debianLexicalCompare("", "~") <= 0 {
		t.Errorf("debianLexicalCompare: `` is greater than `~`")
	}
	if debianLexicalCompare("", "a") >= 0 {
		t.Errorf("debianLexicalCompare: `` is less than `a`")
	}
	if debianLexicalCompare("a", "9") >= 0 {
		t.Errorf("debianLexicalCompare: `a` is less than `9`")
	}
	if debianLexicalCompare("-", "a") <= 0 {
		t.Errorf("debianLexicalCompare: `-` is greater than `a`")
	}
	if debianLexicalCompare(".", "-") <= 0 {
		t.Errorf("debianLexicalCompare: `.` is greater than `-`")
	}
	if debianLexicalCompare("-", "_") >= 0 {
		t.Errorf("debianLexicalCompare: `-` is less than `_`")
	}
}

func TestStrIntCompare(t *testing.T) {
	if strIntCompare("", "0") != 0 {
		t.Errorf("Test zero is the same as the empty string")
	}
	if strIntCompare("00000113", "113") != 0 {
		t.Errorf("Test zero padding doesn't do anything")
	}
	if strIntCompare("19", "2") <= 0 {
		t.Errorf("Test number over lex")
	}
	if strIntCompare("21", "22") >= 0 {
		t.Errorf("Test lex works for same sized strings")
	}
	if strIntCompare("021", "00037") >= 0 {
		t.Errorf("Test randomly")
	}
}
func TestDebianEpoch(t *testing.T) {
	var num string
	var rst string
	num, rst = debianEpoch("0:1")
	if num != "0" {
		t.Errorf("Debian epoch basic: num")
	}
	if rst != "1" {
		t.Errorf("Debian epoch basic: rst")
	}
	num, rst = debianEpoch("15")
	if num != "0" {
		t.Errorf("Debian epoch implied: num")
	}
	if rst != "15" {
		t.Errorf("Debian epoch implied: rst")
	}
	num, rst = debianEpoch("138:15")
	if num != "138" {
		t.Errorf("Debian epoch normal: num")
	}
	if rst != "15" {
		t.Errorf("Debian epoch normal: rst")
	}
}

func TestDebianCompare(t *testing.T) {
	if DebianCompare("", "") != 0 {
		t.Errorf("Test equality of two empty debian versions")
	}
	if DebianCompare("~~", "~~a") >= 0 {
		t.Errorf("Test precedence: `~~` less than `~~a`")
	}
	if DebianCompare("~", "~~a") <= 0 {
		t.Errorf("Test precedence: `~` greater than `~~a`")
	}
	if DebianCompare("~", "") >= 0 {
		t.Errorf("Test precedence: `~` less than `~a`")
	}
	if DebianCompare("a", "") <= 0 {
		t.Errorf("Test precedence: `a` less than ``")
	}
	if DebianCompare("1.2.3~rc1", "1.2.3") >= 0 {
		t.Errorf("Test precedence: `1.2.3~rc1` less than `1.2.3`")
	}
	if DebianCompare("1.2", "1.2") != 0 {
		t.Errorf("Test precedence: `1.2` equal to `1.2`")
	}
	if DebianCompare("1.2", "a1.2") >= 0 {
		t.Errorf("Test precedence: `1.2` equal to `1.2`")
	}
	if DebianCompare("1.2.3", "1.2-3") <= 0 {
		t.Errorf("Test precedence: `1.2.3` is less than `1.2-3`")
	}
	if DebianCompare("1.2.3~rc1", "1.2.3~~rc1") <= 0 {
		t.Errorf("Test precedence: `1.2.3~rc1` is greater than `1.2.3~~rc1`")
	}
	if DebianCompare("1.2.3", "2") >= 0 {
		t.Errorf("Test precedence: `1.2.3` is less than `2`")
	}
	if DebianCompare("0.19.0", "0.2.0") <= 0 {
		t.Errorf("Test precedence: `0.19.0` is greater than `0.2.0`")
	}
	if DebianCompare("2.0.0", "2.0") <= 0 {
		t.Errorf("Test precedence: `2.0.0` is greater than `2.0`")
	}
	if DebianCompare("1.2.a", "1.2.3") <= 0 {
		t.Errorf("Test precedence: `1.2.a` is greater than `1.2.3`")
	}
	if DebianCompare("1.2.a", "1.2a") <= 0 {
		t.Errorf("Test precedence: `1.2.a` is greater than `1.2a`")
	}
	if DebianCompare("1", "1.2.3.4") >= 0 {
		t.Errorf("Test precedence: `1` is less than `1.2.3.4`")
	}
	if DebianCompare("1:2.3.4", "1:2.3.4") != 0 {
		t.Errorf("Test precedence: `1:2.3.4` is equal to `1:2.3.4`")
	}
	if DebianCompare("0:", "0:") != 0 {
		t.Errorf("Test precedence: `0:` is equal to `0:`")
	}
	if DebianCompare("0:", "") != 0 {
		t.Errorf("Test precedence: `0:` is equal to ``")
	}
	if DebianCompare("0:1.2", "1.2") != 0 {
		t.Errorf("Test precedence: `0:1.2` is equal to `1.2`")
	}
	if DebianCompare("0:", "1:") >= 0 {
		t.Errorf("Test precedence: `0:` is less than 1:`")
	}
	if DebianCompare("19:", "2:") <= 0 {
		t.Errorf("Test precedence: `19:` is greater than `2:`")
	}
	if DebianCompare("0:1.2.3", "2:0.4.5") >= 0 {
		t.Errorf("Test precedence: `0:1.2.3` is less than `2:0.4.5`")
	}
}
