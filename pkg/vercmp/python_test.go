package vercmp

import (
	"testing"
)

/*
The following test case versions list is taken from the file
"tests/test_version.py" of the `pypa/packaging` git repository, found here:
https://github.com/pypa/packaging The top of that file carries this copyright
notice:

> This file is dual licensed under the terms of the Apache License, Version
> 2.0, and the BSD License. See the LICENSE file in the root of this repository
> for complete details.
*/
var pythonVersions [55]string = [...]string{
	// Implicit epoch of 0
	"1.0.dev456", "1.0a1", "1.0a2.dev456", "1.0a12.dev456", "1.0a12",
	"1.0b1.dev456", "1.0b2", "1.0b2.post345.dev456", "1.0b2.post345",
	"1.0b2-346", "1.0c1.dev456", "1.0c1", "1.0rc2", "1.0c3", "1.0",
	"1.0.post456.dev34", "1.0.post456", "1.1.dev1", "1.2", "1.2+123abc",
	"1.2+123abc456", "1.2+abc", "1.2+abc123", "1.2+abc123def", "1.2+1234.abc",
	"1.2+123456", "1.2.r32+123456", "1.2.rev33+123456",

	// Explicit epoch of 1
	"1!1.0.dev456", "1!1.0a1", "1!1.0a2.dev456", "1!1.0a12.dev456", "1!1.0a12",
	"1!1.0b1.dev456", "1!1.0b2", "1!1.0b2.post345.dev456", "1!1.0b2.post345",
	"1!1.0b2-346", "1!1.0c1.dev456", "1!1.0c1", "1!1.0rc2", "1!1.0c3", "1!1.0",
	"1!1.0.post456.dev34", "1!1.0.post456", "1!1.1.dev1", "1!1.2+123abc",
	"1!1.2+123abc456", "1!1.2+abc", "1!1.2+abc123", "1!1.2+abc123def",
	"1!1.2+1234.abc", "1!1.2+123456", "1!1.2.r32+123456", "1!1.2.rev33+123456",
}

func helpTestPythonPair(a string, b string, t *testing.T) {
	if PythonCompare(a, b) >= 0 {
		t.Errorf("PythonCompare: `%v` is less than `%v`", a, b)
	}
	if PythonCompare(b, a) <= 0 {
		t.Errorf("PythonCompare: `%v` is greater than `%v`", b, a)
	}
	if PythonCompare(a, a) != 0 {
		t.Errorf("PythonCompare: `%v` is equal to `%v`", a, a)
	}
}

func TestPythonCompare(t *testing.T) {
	if PythonCompare("", "") != 0 {
		t.Errorf("PythonCompare: `` is equal to ``")
	}
	if PythonCompare("1.2.3rc1", "1.2.3RC1") != 0 {
		t.Errorf("PythonCompare: `1.2.3rc1` is less than `1.2.3RC1`")
	}
	if PythonCompare("1.0+foo0100", "1.0+foo100") >= 0 {
		t.Errorf("PythonCompare: `1.0+foo0100` is less than `1.0+foo100`")
	}
	if PythonCompare("1.0+0100foo", "1.0+100foo") >= 0 {
		t.Errorf("PythonCompare: `1.0+0100foo` is less than `1.0+100foo`")
	}
	if PythonCompare("1.0.a1", "1.0a1") != 0 {
		t.Errorf("PythonCompare: `1.0.a1` is less than `1.0a1`")
	}
	if PythonCompare("1.0.rc1", "1.0rc1") != 0 {
		t.Errorf("PythonCompare: `1.0.rc1` is less than `1.0rc1`")
	}
	if PythonCompare("1.0.b1", "1.0b1") != 0 {
		t.Errorf("PythonCompare: `1.0.b1` is less than `1.0b1`")
	}
	for i := 1; i < len(pythonVersions); i++ {
		helpTestPythonPair(pythonVersions[i-1], pythonVersions[i], t)
	}
}
