// Package vercmp provides many different string-based version comparison
// algorithms, intended to be used a la `strings.Sort`.
package vercmp

import (
	"regexp"
	"strings"
)

var findZeroes *regexp.Regexp = regexp.MustCompile("^0+")

func strIntCompare(a string, b string) int {
	aNormalized := findZeroes.ReplaceAllString(a, "")
	bNormalized := findZeroes.ReplaceAllString(b, "")
	if len(aNormalized) == 0 {
		if len(bNormalized) == 0 {
			return 0
		} else {
			return -1
		}
	} else if len(bNormalized) == 0 {
		return 1
	} else if len(aNormalized) == len(bNormalized) {
		return strings.Compare(aNormalized, bNormalized)
	} else {
		return len(aNormalized) - len(bNormalized)
	}
}
